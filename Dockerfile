FROM openjdk:8
EXPORT 8081
ADD target/ProjectManager-docker.jar ProjectManager-docker.jar
ENTRYPOINT ["java","-jar","/ProjectManager-docker.jar"]


