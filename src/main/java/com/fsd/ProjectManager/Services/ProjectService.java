package com.fsd.ProjectManager.Services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fsd.ProjectManager.Models.Project;
import com.fsd.ProjectManager.Models.User;
import com.fsd.ProjectManager.Repo.ProjectRepository;

@Service
public class ProjectService {
	@Autowired
	ProjectRepository prjRepo;
	
	public Iterable<Project> getProjects(){
		Iterable<Project> prjList=new ArrayList<>();
		
		prjList= prjRepo.findAll();
		return prjList;
	}
	
	public Project addProject(Project project) {
		
		return prjRepo.save(project);
	}
	
	public void updateProject(Project project) {
	
		prjRepo.save(project);
	}
	
	public void deleteProject(Project project) {
		prjRepo.delete(project);
	}
}
