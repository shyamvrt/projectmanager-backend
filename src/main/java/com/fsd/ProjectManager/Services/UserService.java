package com.fsd.ProjectManager.Services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fsd.ProjectManager.Models.User;
import com.fsd.ProjectManager.Repo.UserRepository;
@Service
public class UserService {
	
	 @Autowired
	 UserRepository userRepo;
	
	
	public Iterable<User> getAllUser() {
		Iterable<User> userList=new ArrayList<>();
	   
		userList=userRepo.findAll();
		return userList;
		
	}
	
	public Optional<User> getAUser(long id) {
		return userRepo.findById(id);
	}
	
	public void updateUser(User user) {
		 userRepo.save(user);
		
	}
	
	public User addUser(User user) {
		 return userRepo.save(user);
		
	}

	public void deleteUser(User user) {
		
		userRepo.delete(user);
	}
	public Optional<User> findById(long id) {
	return	userRepo.findById(id);
	}
	
	
}
