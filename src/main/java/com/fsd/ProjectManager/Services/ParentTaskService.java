package com.fsd.ProjectManager.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fsd.ProjectManager.Models.ParentTask;
import com.fsd.ProjectManager.Repo.ParentTaskRepository;

@Service
public class ParentTaskService {
	@Autowired
	private ParentTaskRepository parentTaskRepository;
	
	public List<ParentTask> getAllParentTasks(){
		return (List<ParentTask>) parentTaskRepository.findAll();
	}
	
	public void addParentTask (ParentTask parentTask) {
		 parentTaskRepository.save(parentTask);
	}
	

}
