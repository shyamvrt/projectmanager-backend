package com.fsd.ProjectManager.Repo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.fsd.ProjectManager.Models.User;
@Repository
public interface UserRepository extends CrudRepository<User, Long> {

}
