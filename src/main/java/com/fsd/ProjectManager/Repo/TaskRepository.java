package com.fsd.ProjectManager.Repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.fsd.ProjectManager.Models.Task;

@Repository
public interface TaskRepository extends CrudRepository<Task,Long>{

	
}
