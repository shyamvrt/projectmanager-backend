package com.fsd.ProjectManager.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fsd.ProjectManager.Models.Project;
import com.fsd.ProjectManager.Repo.ProjectRepository;
import com.fsd.ProjectManager.Services.ProjectService;

@RestController
@CrossOrigin(origins ="*")

public class ProjectController {
	@Autowired
	ProjectService projectService;
	
	@GetMapping("/viewProjects")
	public Iterable<Project> getAllProjects() {
		
		return projectService.getProjects();
	}
	
	
	@PostMapping("/addProject")
	public Project addProject(@RequestBody Project project) {
	     System.out.println("amingo");
		return projectService.addProject(project);
	}

	
	@PostMapping("/deleteProject")
	public void deleteProject(@RequestBody Project project) {
		
		 projectService.deleteProject(project);
	}

}
