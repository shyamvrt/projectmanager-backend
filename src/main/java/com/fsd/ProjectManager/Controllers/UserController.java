
package com.fsd.ProjectManager.Controllers;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fsd.ProjectManager.Models.User;
import com.fsd.ProjectManager.Services.UserService;
@RestController
@CrossOrigin(origins ="*")
public class UserController {
	@Autowired
	UserService  userService;
	
	@GetMapping("/viewUsers")
	public Iterable<User> getAllUser(){
		return userService.getAllUser();
		}

	
	
	@PostMapping("/addUser")
	public User addUser(@RequestBody User user) {
		return userService.addUser(user);
	}
	
	@GetMapping("/viewUser/{id}")
	public Optional<User> getAUser(@PathVariable long id) {
		return userService.getAUser(id);
		
	}
	
	@PutMapping("/updateUser")
	public void updateUser(@RequestBody User user) {
		 userService.updateUser(user);
	}
	
	
	
	@PostMapping("/deleteUser")
	public void deleteUser(@RequestBody User user) {
		 userService.deleteUser(user);
		
	}
	
	
	
	
	
}
