package com.fsd.ProjectManager.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fsd.ProjectManager.Models.Task;
import com.fsd.ProjectManager.Services.TaskService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class TaskController {
	@Autowired
	TaskService taskService;
	

	@GetMapping("/viewTasks")
	public List<Task> getAllTasks() {

		return taskService.viewAllTask();

	}
	
	

	@PostMapping("/addTask")
	public void addTask(@RequestBody Task task) {
	System.out.println("hola");
	
		taskService.addTask(task);

	}
}
