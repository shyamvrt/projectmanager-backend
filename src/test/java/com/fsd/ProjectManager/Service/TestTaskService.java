package com.fsd.ProjectManager.Service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.fsd.ProjectManager.Models.ParentTask;
import com.fsd.ProjectManager.Models.Project;
import com.fsd.ProjectManager.Models.Task;
import com.fsd.ProjectManager.Models.User;
import com.fsd.ProjectManager.Repo.ParentTaskRepository;
import com.fsd.ProjectManager.Repo.TaskRepository;
import com.fsd.ProjectManager.Services.TaskService;

public class TestTaskService{
	@InjectMocks
	TaskService taskservice;

	@Mock
	TaskRepository taskrepo;

	@Mock
	ParentTaskRepository Ptrepo;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
//    @Test 
//    public void getParentTaskTest() throws ParseException {
//    	ParentTask pt1 = new ParentTask(0,"No parent task");
//    	ParentTask pt2=new ParentTask(1,"develop");
//    	List<ParentTask> parenttask = new ArrayList<ParentTask>();
//		parenttask.add(pt1);
//		parenttask.add(pt2);
//		when(Ptrepo.findAll()).thenReturn(parenttask);
//		// test
//		List<ParentTask> ptList = taskservice.getParentTask();
//		assertEquals(2, ptList.size());
//		verify(Ptrepo, times(1)).findAll();
//    }
    @Test 
    public void getTaskTest() throws ParseException {
    	 
    	  LocalDate startdate1 = LocalDate.of(2017, 1, 13);
		  LocalDate startdate2 = LocalDate.of(2017, 2, 13);
		  LocalDate enddate1 = LocalDate.of(2017, 3, 13);
		  LocalDate enddate2 = LocalDate.of(2017, 4, 13);
			Task t1 = new Task(new Project("amex", startdate1, enddate1, 5, new User("firstuser1","lastuser1",1234),false,2,1),"code",8,new ParentTask(1,"develop"),startdate1, enddate1, new User("firstuser1","lastuser1",1234),1,false);
			
			
			List<Task> task = new ArrayList<Task>();
			task.add(t1);
			
			when(taskrepo.findAll()).thenReturn(task);
			// test
			List<Task> taskList = taskservice.viewAllTask();
			assertEquals(1, taskList.size());
			verify(taskrepo, times(1)).findAll();
    }

}