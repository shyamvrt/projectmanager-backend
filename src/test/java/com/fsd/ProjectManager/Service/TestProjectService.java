package com.fsd.ProjectManager.Service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.fsd.ProjectManager.Models.Project;
import com.fsd.ProjectManager.Models.User;
import com.fsd.ProjectManager.Repo.ProjectRepository;
import com.fsd.ProjectManager.Services.ProjectService;

public class TestProjectService {

	

	@InjectMocks
	ProjectService projecttestservice;
	
	@Mock
	ProjectRepository projecttestrepo;
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	  @Test 
		public void getAllProjectTest() throws ParseException {
		  LocalDate startdate1 = LocalDate.of(2017, 1, 13);
		  LocalDate startdate2 = LocalDate.of(2017, 2, 13);
		  LocalDate enddate1 = LocalDate.of(2017, 3, 13);
		  LocalDate enddate2 = LocalDate.of(2017, 4, 13);
			Project project1 = new Project("amex", startdate1,enddate1,22,new User("firstuser2","lastuser2",4567),
					true,3,1);
			Project project2 = new Project("amex", startdate2,enddate2,23,new User("firstuser1","lastuser1",1234),
					true,3,2);
			List<Project> project = new ArrayList<Project>();
			project.add(project1);
			project.add(project2);
			when(projecttestrepo.findAll()).thenReturn(project);
			
			List<Project> projectList = (List<Project>) projecttestservice.getProjects();
			assertEquals(2, projectList.size());
			verify(projecttestrepo, times(1)).findAll();
			 
	  }
	public void createProjectTest() throws ParseException {
		LocalDate startdate1 = LocalDate.of(2017, 1, 13);
		  LocalDate enddate1 = LocalDate.of(2017, 2, 13);
			Project project1 = new Project("amex", startdate1,enddate1,22,new User("firstuser2","lastuser2",4567),
					true,3,1);
			projecttestservice.addProject(project1);
			verify(projecttestrepo, times(1)).save(project1);
	}


}
